# Arduino for SensorApp

SensorApp application needs some data to come in through an Arduino board. This is meant to record some data coming from constraints probes.

This code makes the Arduino board a slave of SensorApp.

## Prerequisite

* Arduino board,
* constraints probes and all their dependencies (apmlifier, etc.).

## How to use it

Assemble the electrical circuit and upload the "Mesure" code on the Arduino. You can run the "Calibration" code before that if you want and if you can calibrate the probes.

The script "Mesure_test_PC" is to test the circuit by connecting the Arduino board to a computer. It is for test purpose on the constraint probes.

Run SensorApp on an Android device and connect the Arduino to it. Start the app.

Check the [SensorApp documentation](https://gitlab.imt-atlantique.fr/commande-entreprise-20/sensorapp) for further information.