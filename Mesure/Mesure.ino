/*
 Arduino pin 2 -> HX711 CLK
 3 -> DAT
 5V -> VCC
 GND -> GND
*/

#include "HX711.h"

HX711 probe1;
HX711 probe2;
HX711 probe3;
HX711 probe4;
HX711 probe5;
HX711 probe6;
int probes[] = {probe1, probe2, probe3, probe 4, probe5, probe6};

// PARAMETERS TO SET DEPENDING ON YOUR CIRCUIT

#define calibration_factor -7050.0 // Change it if you used the "Calibration" script

int nbProbes = 1; // Numbre of probes connected to the board

// Pins used by the probes
int DOUTs[] = {3, 0, 0, 0, 0, 0};
int CLKs[] = {2, 0, 0, 0, 0, 0};

void setup() {
  Serial.begin(9600);

  /*
    Instructions to initialize the probe objects :
    Makes nbProbes probe objects working.
  */
  for (int i = 0; i < nbProbes; i++) {
      probes[i].begin(DOUTs[i], CLKs[i]);
      probes[i].set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
      probes[i].tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0
  }
}

void loop() {
  // Reading order is given by Cordova (by the serial bus)
  if (Serial.available() > 0) {
    // Using the library to read the values of the probes
    for (int i = 0; i < nbProbes; i++) {
      Serial.println(probes[i].get_units(), 1); //scale.get_units() returns a float
    }

    // Making the buffer empty befire receiving a new order from Cordova
    while (Serial.available() > 0) {
      Serial.read();
    }
  }
}
